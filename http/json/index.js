const http = require("http");

function onRequest(request, response) {
  response.writeHead(200, { "Content-Type": "application/json" });
  const data = {
    name: "Sabrina",
    age: 24,
    gender: "Female",
  };
  response.end(JSON.stringify(data));
}

http.createServer(onRequest).listen(8000);
