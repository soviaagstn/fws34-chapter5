function luasSegitiga(alas, tinggi) {
  return (alas * tinggi) / 2;
}

// Export module luasSegitiga agar dapat dipakai di file lain
module.exports = luasSegitiga;
