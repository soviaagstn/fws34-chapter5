// Install third party module -> npm i superheroes
const superheroes = require("superheroes");

// Install third party module -> npm i supervillains
const supervillains = require("supervillains");

// Install third party module -> npm i cat-names
const catNames = require("cat-names");

// Methods random to get random name of superhero
const mySuperheroName = superheroes.random();

// Methods random to get random name of supervillain
const mySupervillainName = supervillains.random();

// Methods random to get random name of cat
const myCatName = catNames.random();

console.log(mySuperheroName);
console.log(mySupervillainName);
console.log(myCatName);

// Import module luasSegitiga (local module, module yang dibuat sendiri)
const luasSegitiga = require("../export/export.js");

console.log(luasSegitiga(6, 8));
