// Import module fs (core module from Node.js)
const fs = require("fs");

// Read file text.txt
const text = fs.readFileSync("./text.txt", "utf-8");

// Print output in terminal
console.log(text);
